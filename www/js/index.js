// 1. create a global database variable
var db = null;

// add event listeners
document.addEventListener("deviceReady", connectToDatabase);
document.getElementById("insertHeroes").addEventListener("click", insertHeroes);
document.getElementById("showAllHeroes").addEventListener("click", showAllHeroes)
document.getElementById("rescue").addEventListener("click", rescue)

function connectToDatabase() {
  console.log("device is ready - connecting to database");
  // 2. open the database. The code is depends on your platform!
  if (window.cordova.platformId === 'browser') {
    console.log("browser detected...");
    // For browsers, use this syntax:
    //  (nameOfDb, version number, description, db size)
    // By default, set version to 1.0, and size to 2MB
    db = window.openDatabase("superdb", "1.0", "Database for SuperHeroes app", 2*1024*1024);
  }
  else {
    alert("mobile device detected");
    console.log("mobile device detected!");
    var databaseDetails = {"name":"superdb.db", "location":"default"}
    db = window.sqlitePlugin.openDatabase(databaseDetails);
    console.log("done opening db");
  }

  if (!db) {
    alert("databse not opened!");
    return false;
  }

  // 3. create relevant tables
  db.transaction(createTables)

}

function createTables(transaction) {
  var sql = "CREATE TABLE IF NOT EXISTS heroes (id integer PRIMARY KEY AUTOINCREMENT, name text, isAvailable integer)"
  transaction.executeSql(sql, [], createSuccess, createFail)
}

function createSuccess(tx, result) {
  alert("Table created! " + JSON.stringify(tx));
  alert("Result: " + JSON.stringify(result));
}
function createFail(error) {
  alert("Failure while creating table: " + error);
}

//insert
function insertHeroes(transaction) {
  db.transaction(function (transaction) {
      // save the values to the database
      db.transaction(function(transaction) {
        transaction.executeSql("SELECT * FROM heroes WHERE name = ? "+
          "OR name = ? "+
          "OR name = ? "+
          "OR name =? ", ["Spiderman", "Thor", "Captain America", "Wonder Woman"],
          function (tx, results) {
            var numRows = results.rows.length;
            if (numRows<1) {
                var sql = "INSERT INTO heroes (name, isAvailable) VALUES (?,?), (?,?), (?,?), (?,?)"
                transaction.executeSql(sql,["Spiderman", 1, "Thor", 1, "Captain America", 0, "Wonder Woman", 0], function(tx,result){
                }, function(error){
                  alert("Insert failed: " + error);
                });
            }
          }, function(error){
            alert("An error occured: " + error)
          });
      });
    }
  );
}

//show
function showAllHeroes() {
  // clear the user interface
  document.getElementById("heroes").innerHTML = "";

  db.transaction(function(transaction) {
    transaction.executeSql("SELECT * FROM heroes", [],
      function (tx, results) {
        var numRows = results.rows.length;

        for (var i = 0; i < numRows; i++) {

          // to get individual items:
          var item = results.rows.item(i);
          console.log(item);
          console.log(item.name);

          var available = "No";
          if(item.isAvailable == 1){
              available = "Yes";
          }else {
              available = "No";
          }
          // show it in the user interface
          document.getElementById("heroes").innerHTML +=
              "<p>Name: " + item.name + "</p>"
            + "<p>Available To Hire: " + available + "</p>"
            + "<br>";
        }

      }, function(error){
        alert("An error occured: " + error)
      });
  });
}

//Rescue
function rescue(){
    navigator.vibrate(3000);
}
